#!/usr/bin/env sh

## Default settings
BUILD_DIR="$(pwd)/build"
USECOLOUR="yes"
REPO_BASE="$(pwd)/repos"
BUILDER_EXTRA_OPTS=""
GPGKEY=""
TESTMODE="no"
PKG=""
MANIFEST_DIR="$(pwd)/manifests"
REPOTITLE=""
REPOURL=""
REPOHOMEPAGE=""
REPOCOMMENT=""
REPODESCRIPTION=""
REPOICON=""
REPOGPGKEYFILE=""

SCRIPTNAME="$(basename $0)"
CD="$(pwd)"
cd_last_dir="$(echo $CD | awk -F / '{print $NF}')"


## Define colours used
function setcolour {
	if [ "$USECOLOUR" = "yes" ]
	then
		red="\e[31m"
		bred="\e[1;31m"
		green="\e[32m"
		bgreen="\e[1;32m"
		yellow="\e[33m"
		byellow="\e[1;33m"
		none="\e[0m"
		reset=$none
	else
		red=""
		bred=""
		green=""
		bgreen=""
		yellow=""
		byellow=""
		none=""
		reset=$none
	fi
}

function usage {
	echo -e "Usage: ${green}scripts/$SCRIPTNAME${reset} [build options] <package.to.build>"
	echo -e "Where the manifest file is the filename in repos/ without '.json'"
	echo -e "Default settings:"
	echo -e "\tUse Colour: Yes"
	echo -e "\tBuild Directory: ${BUILD_DIR}"
	echo -e "\tRepo Directory: ${REPO_BASE}"
	echo -e "\tManifest Directory: ${MANIFEST_DIR}"
	echo -e "\tGPG Key: none"
	echo -e "\tTestmode: no"
	echo -e ""
	echo -e "Options:"
	echo -e "\t-h : Print this usage message and exit"
	echo -e "\t-l : List available packages"
	echo -e "\t-n : Don't use colour"
	echo -e "\t-t : Enable test mode. Prints the command that will run without running the command."
	echo -e "\t-g \"<gpg-key-id>\" : Set the GPG key to use. If not set, defaults to not signing."
	echo -e "\t-b \"<directory>\" : Set base build directory to <directory>"
	echo -e "\t-r \"<directory>\" : Set the base directory for the repositories"
	echo -e "\t-m \"<directory>\" : Set the manifest directory to find the package to build."
	echo -e "\t-f \"<Extra options>\" : Set extra options for flatpak builder. Must be enclosed in \"\""
	echo -e ""
}

function get_manifest_file {
	local pkg=$1
	local manifest="$MANIFEST_DIR/$pkg.json"
	[ -f "$manifest" ] && echo "$manifest" || echo "err"
}

function list_avail {
	PKG_AVAIL="$(ls $MANIFEST_DIR/*.json | sed -e "s@$MANIFEST_DIR/@@g" -e "s@.json@@g")"
	echo -e "$PKG_AVAIL"
	exit 0
}

setcolour

if [ "$cd_last_dir" = "scripts" ]
then
	right_dir=$(pwd | sed "s@/$cd_last_dir@@")
	echo -e "${bred}Switching to $right_dir and re-running script...${reset}"
	cd $right_dir
	./scripts/$SCRIPTNAME $@
	code=$?
	if [ $code -eq 0 ]
	then
		echo -e "${bgreen}Done running script in $right_dir${reset}"
	else
		echo -e "${bred}Error running script in $right_dir${reset}"
	fi
	exit $code
fi

while getopts "hltnm:b:r:g:f:" opt
do
	case $opt in
		n)
			USECOLOUR="no"
			setcolour
			;;
		m)
			MANIFEST_DIR="$OPTARG"
			;;
		b)
			BUILD_DIR="$OPTARG"
			;;
		r)
			REPO_BASE="$OPTARG"
			;;
		g)
			GPGKEY="$OPTARG"
			;;
		t)
			TESTMODE="yes"
			;;
		f)
			BUILDER_EXTRA_OPTS="$OPTARG"
			;;
		l)
			DO_LIST="yes"
						;;
		h)
			usage
			exit 0
			;;
		*)
			usage
			exit 1
			;;
	esac
done

if [ "$DO_LIST" = "yes" ]
then
	list_avail
fi

for last; do true; done
PKG=$last

if [ -z "$PKG" ]
then
	echo -e "${bred}Manifest file not defined."
	usage
	exit 1
fi

MANIFEST=$(get_manifest_file $PKG)
buildin=$BUILD_DIR/$PKG
REPO=$REPO_BASE/$PKG

echo -e "${green}Welcome to the flatpak builder script!${reset}"

if [ "$TESTMODE" = "yes" ]
then
	echo -e "${byellow}Running in test mode...${reset}"
	echo -e ""
fi

if [ "$(get_manifest_file $PKG)" = "err" ]
then
	echo -e "${bred}Error: Cannot locate manifest file for $PKG${reset}"
	exit 2
fi

echo -e "${yellow}Building: ${green}$PKG${reset}"
echo -e "${yellow}Manifest: ${green}$MANIFEST${reset}"
echo -e "${yellow}Build Directory: ${green}$buildin${reset}"
echo -e "${yellow}Using Repository: ${green}$REPO${reset}"
if [ -z "$GPGKEY" ]
then
	echo -e "${yellow}GPG Key is not set${reset}"
else
	echo -e "${yellow}GPG Key: ${green}$GPGKEY${reset}"
fi

if [ ! -z "$BUILDER_EXTRA_OPTS" ]
then
	echo -e "${yellow}Extra options for flatpak-builder: ${green}$BUILDER_EXTRA_OPTS${reset}"
fi

## Build command line
cmd="flatpak-builder"

if [ ! -z "$GPGKEY" ]
then
	cmd+=" --gpg-sign=${GPGKEY}"
fi

cmd+=" --force-clean"
cmd+=" --repo=${REPO}"

if [ ! -z "$BUILDER_EXTRA_OPTS" ]
then
	cmd+=" $BUILDER_EXTRA_OPTS"
fi

cmd+=" $buildin $MANIFEST"

if [ "$TESTMODE" != "yes" ]
then
	echo -e "${yellow}Running command: ${green}$cmd${reset}"
	$cmd
	echo $?
else
	echo -e "${yellow}Testing command: ${green}$cmd${reset}"
	echo -e ""
	echo -e "${byellow}Exiting test mode...${reset}"
	exit 0
fi
